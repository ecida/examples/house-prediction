#!/usr/bin/env bash

set -e
set -o pipefail

# Set the Internal Field Separator to a newline, so that we can create arrays
# line by line instead of word by word
IFS=$'\n'

# Calculate the set of resources that we currently have
desired=( $(yq '.' dist/*.yaml -o json | jq -c '.metadata | { namespace, name }') )

# Calculate the set of currently deployed resources
current=( $(kubectl get applications -l applied-by=cicd -A -o json | jq -c ".items[].metadata | { namespace, name }") )

echo "Desired resources"

for d in "${desired[@]}"; do
    echo $d
done

echo "Current resources on the cluster"

for d in "${current[@]}"; do
    echo $d
done

# Subtract the desired from the current to get the resources that need to be deleted
deletethis=(${current[@]})

for d in ${desired[@]}; do
    deletethis=( "${deletethis[@]/$d}" )
done

echo "Deleting undeclared resources"
for d in ${deletethis[@]}; do
    echo "Deleting $d"

    # TODO: fix name injection exploit with creative names
    echo $d | jq --raw-output '"kubectl -n " + .namespace + " delete applications.ecida.org " + .name' | sh
done
