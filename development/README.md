# Development environment

This directory represents the development environment for ECiDA. Note that the
only resource that the user creates is the application resource. All other
resources that are listed here will be created (and kubectl applied) by the
operator.

## Configuration values

These resources are originally uploaded to the repository with some default
configuration values. Specifically, the ./module-predictor.yaml has an
environment variable `MODULE_VERSION` set to "v1" by default.

In the pipeline template that also lives in the repository, this default value
is overridden to be "v2". Ultimately at the highest level, the user deploys an
application where this value is set to "v3". Since this is the highest level of
configuration that one can provide, this is the value that is chosen.
Therefore, the value in the module that is deployed (which is what this
directory is about) is "v3".
